Welcome to The 

Galactic
Orbital
Positioning
System

G.O.P.S!

Run GOPS.py to start your navigation.
Input your craft's statistics and your current trajectory, then input your desired trajectory and sit back while GOPS calculates your needed burn duration and angle. 

Travelling the stars has never been so easy!

Details:
- Craft units should be in kg and N
- Trajectory units should be in km from Earth's surface and degrees
- Yes/No inputs should be responded to as "y" or "n" without quotes