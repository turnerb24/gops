##GLOBAL VARIABLES
import Globals
import math
##FUNCTION DEFINITIONS
def wipe():
    spacer(100)

def spacer(n):
    print("\n"*n)

def pause():
    str = input()
    while str != "":
        str = input()

def opener():
    wipe()
    print("Welcome to the Galactic Orbital Position System!")
    print("Please press ENTER to begin")
    pause()

def optionalCraftInfo() :
    print("Are you flying a craft other than the SpaceX Dragon 2?")
    custom = input("y/n\t:")
    if custom == "y" :
        ##stuff
        spacer(1)
        print("Please enter the following information using SI units (kg, kN")
        Globals.mass = input("Mass\t:")
        Globals.thrust = input("Thrust\t:")
        print("Great! Lets begin navigating!")
    else :
        Globals.mass = 4200
        Globals.thrust = 584
        print("Then let's begin navigating!")
    updateDVPS()

def updateDVPS() :
    Globals.dvps = float(Globals.thrust)/float(Globals.mass)

def calcDV() :
    vs = math.sqrt((float(Globals.Gconst) * float(Globals.EarthMass)) / (float(Globals.calt) + float(Globals.EarthRadius)))
    vhs = math.cos(float(Globals.cincl)) * vs
    vvs = math.sin(float(Globals.cincl)) * vs
    print("Vs is " + str(vs))
    ve = math.sqrt((float(Globals.Gconst) * float(Globals.EarthMass)) / (float(Globals.dalt) + float(Globals.EarthRadius)))
    vhe = math.cos(float(Globals.dincl)) * ve
    vve = math.sin(float(Globals.dincl)) * ve
    print("Ve is " + str(ve))
    Globals.dX = vhe - vhs
    Globals.dY = vve - vvs
    Globals.dV = math.sqrt(float(Globals.dX)**(2) + float(Globals.dY)**(2))
    Globals.bTime = float(Globals.dV) / float(Globals.dvps)

def calcBAngle() :
    if (Globals.dV != 0) :
        Globals.bAngle = math.acos(math.fabs(Globals.dX/Globals.dV))
    else :
        Globals.bAngle = 0
def printBurnCourse() :
    print("Your total Delta-V is: " + str(Globals.dV))
    print("\tdX: " + str(Globals.dX))
    print("\tdY: " + str(Globals.dY))
    print("You need to burn at an inclination of " + str(Globals.bAngle) + " for " + str(Globals.bTime) + " seconds.")


def readCurrent():
    print("Please input the following information as numerical answers")
    print("Note: Altitudes should be in km relative to Earth's surface, other numbers in degrees")
    spacer(1)
    print("CURRENT TRAJECTORY")
    Globals.calt = input("Altitude\t\t:")
    degInc = input("Inclination\t\t:")
    Globals.cincl = float(degInc) / 14.324 # degrees to radians
    spacer(1)

def printCurrentTraj() :
    print(Globals.calt)
    print(Globals.casc)
    print(Globals.cincl)

def readTarget():
    print("Please input the following information as numerical answers")
    print("Note: Altitudes should be in km relative to Earth's surface, other numbers in degree")
    spacer(1)
    print("DESIRED TRAJECTORY")
    Globals.dalt = input("Altitude\t\t:")
    degInc = input("Inclination\t\t:")
    Globals.dincl = float(degInc) / 14.324
    spacer(1)

def printTargetTraj() :
    print(Globals.dalt)
    print(Globals.dasc)
    print(Globals.dincl)

def presentState() :
    print("Current craft and trajectory:")
    print("Mass\t:", str(Globals.mass))
    print("Thrust\t:", str(Globals.thrust))
    print("Delta-V per second", str(Globals.dvps))
    spacer(1)
    printCurrentTraj()

##MAIN SCRIPT
opener()
spacer(2)
optionalCraftInfo()
spacer(1)
readCurrent()
spacer(2)
while (Globals.repeat == "y") :
    readTarget()
    spacer(2)
    print("Press ENTER to chart a burn-course")
    pause()
    calcDV()
    calcBAngle()
    printBurnCourse()
    Globals.repeat = input("Would you like to change trajectory again?\nRepeat (y/n):")
    if (Globals.repeat == "y") :
        Globals.calt = Globals.dalt
        Globals.cincl = Globals.dincl
        spacer(2)
    else :
        wipe()
        print("Enjoy your orbit!")
        exit(0)

