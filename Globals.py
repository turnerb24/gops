##Global Variables
#flow vars
repeat = "y"
#craft vars
mass = 4200
thrust = 584
dvps = mass/thrust
#current traj. vars
calt = 0
casc = 0
cincl = 0
#dest. traj. vars
dalt = 0
dasc = 0
dincl = 0
#nav. vars
dX = 0
dY = 0
dV = 0
bAngle = 0
btime = 0
EarthMass = (5.972 * (10**(24)))
EarthRadius = 6371.393
Gconst = (6.67408 * (10**(-11)))
tgtMass = EarthMass
tgtRadius = EarthRadius